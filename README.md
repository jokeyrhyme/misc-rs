# misc-rs

miscellaneous Rust code, for use across my personal projects

we're using a `cargo` workspace in a single repository here,
and choosing not to publish to [crates.io](https://crates.io),
to minimise overhead

## getting started

- TODO: instructions for use via `git submodule`

### Cargo.toml

identify the desired crate(s) and add them to your [Cargo.toml](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html),
taking care to specify the `git` commit, e.g.

```
[dependencies]
combinations = { git = "https://gitlab.com/jokeyrhyme/misc-rs.git", rev = "dd6d5cba348f2a8493f3011bef859c32e56126bc" }
```
