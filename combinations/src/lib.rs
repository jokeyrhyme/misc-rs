#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use std::{fmt::Debug, ops::RangeInclusive};

/// see: [Combination](https://en.wikipedia.org/wiki/Combination)
pub struct CombineForAllK<T>
where
    T: Clone,
{
    all_k: RangeInclusive<usize>,
    combine_for_k: CombineForK<T>,
    elements: Vec<T>,
}
impl<T> CombineForAllK<T>
where
    T: Clone,
{
    fn new(elements: &[T]) -> Self {
        let mut all_k = 1..=elements.len();
        // expect: panic if we were given an empty set of elements
        let k = all_k.next().expect("set of elements must not be empty");
        Self {
            all_k,
            combine_for_k: combine_for_k(elements.iter().cloned(), k),
            elements: elements.to_vec(),
        }
    }
}
impl<T> Iterator for CombineForAllK<T>
where
    T: Clone,
{
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.combine_for_k.next() {
            Some(c) => Some(c),
            None => match self.all_k.next() {
                Some(k) => {
                    self.combine_for_k = combine_for_k(self.elements.iter().cloned(), k);
                    self.combine_for_k.next()
                }
                None => None,
            },
        }
    }
}

pub fn combine_for_all_k<T>(elements: impl Iterator<Item = T>) -> impl Iterator<Item = Vec<T>>
where
    T: Clone,
{
    CombineForAllK::new(elements.collect::<Vec<_>>().as_slice())
}

/// see: [Combination](https://en.wikipedia.org/wiki/Combination)
#[derive(Debug)]
pub struct CombineForK<T> {
    elements: Vec<T>,
    indices: Option<Vec<usize>>,
    n: usize,
    k: usize,
}
impl<T> CombineForK<T> {
    fn new(elements: Vec<T>, k: usize) -> Self {
        let n = elements.len();
        Self {
            elements,
            indices: Some((0..k).collect::<Vec<_>>()),
            n,
            k,
        }
    }
}
// cannot implement ExactSizeIterator,
// as the number of possible combinations may exceed what can be represented by a usize
impl<T> Iterator for CombineForK<T>
where
    T: Clone,
{
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.k > self.n {
            return None;
        }

        let indices = self.indices.as_mut()?;

        let item = indices.iter().map(|i| self.elements[*i].clone()).collect();

        let mut incremented = false;
        // work on indices from last to first
        let mut next_indices = indices.iter().copied().rev().collect::<Vec<_>>();
        for (i, index_in_elements) in next_indices.iter_mut().enumerate() {
            if i == 0 {
                // remember, we reversed, looking at the last index i.e. k-1 in [0 .. k-1]
                let mut maximum = self.n - 1;
                if index_in_elements < &mut maximum {
                    *index_in_elements += 1;
                    incremented = true;
                    break;
                }
            } else {
                // remember, we reversed, so to look at next we need to choose the previous
                let next_index_in_elements = indices[self.k - i];
                let mut maximum = next_index_in_elements - 1;
                if index_in_elements < &mut maximum {
                    *index_in_elements += 1;
                    // expect: indices[i=0] has the highest value,
                    // and we know we're looking at indices[i!=0] and that it must be lower,
                    // so it should always be safe to set indices[i=0] to a relative value
                    next_indices[i - 1] = index_in_elements.checked_add(1).expect(
                        "contraints violated when calculating indices for next combination",
                    );
                    incremented = true;
                    break;
                }
            }
        }
        if incremented {
            next_indices.reverse();
            self.indices = Some(next_indices);
        } else {
            self.indices = None;
        }

        Some(item)
    }
}

pub fn combine_for_k<T>(elements: impl Iterator<Item = T>, k: usize) -> CombineForK<T> {
    CombineForK::new(elements.collect(), k)
}

#[cfg(test)]
mod tests {
    use super::{combine_for_all_k, *};

    #[test]
    fn combine_for_all_k_elements() {
        // choosing input values that cannot be mistaken for indices
        let input: Vec<i32> = vec![10, 20, 30];

        let got = combine_for_all_k(input.iter().copied()).collect::<Vec<Vec<_>>>();

        assert_eq!(
            got,
            vec![
                vec![10],
                vec![20],
                vec![30],
                vec![10, 20],
                vec![10, 30],
                vec![20, 30],
                vec![10, 20, 30]
            ]
        );
    }

    #[test]
    fn combine_for_k_empty() {
        let input: Vec<i32> = vec![];
        let want: Vec<Vec<i32>> = vec![];
        let got = combine_for_k(input.iter().copied(), 5).collect::<Vec<_>>();

        assert_eq!(got, want);
    }

    #[test]
    fn combine_for_k_3_elements() {
        // choosing input values that cannot be mistaken for indices
        let input: Vec<i32> = vec![10, 20, 30];

        let got = combine_for_k(input.iter().copied(), 1);
        let got = got.collect::<Vec<_>>();
        assert_eq!(got, vec![vec![10], vec![20], vec![30]]);

        let got = combine_for_k(input.iter().copied(), 2);
        let got = got.collect::<Vec<_>>();
        assert_eq!(got, vec![vec![10, 20], vec![10, 30], vec![20, 30]]);

        let got = combine_for_k(input.iter().copied(), 3);
        let got = got.collect::<Vec<_>>();
        assert_eq!(got, vec![vec![10, 20, 30]]);
    }

    #[test]
    fn combine_for_k_4_elements() {
        // choosing input values that cannot be mistaken for indices
        let input: Vec<i32> = vec![10, 20, 30, 40];

        let got = combine_for_k(input.iter().copied(), 1);
        let got = got.collect::<Vec<_>>();
        assert_eq!(got, vec![vec![10], vec![20], vec![30], vec![40]]);

        let got = combine_for_k(input.iter().copied(), 2);
        let got = got.collect::<Vec<_>>();
        assert_eq!(
            got,
            vec![
                vec![10, 20],
                vec![10, 30],
                vec![10, 40],
                vec![20, 30],
                vec![20, 40],
                vec![30, 40]
            ]
        );

        let got = combine_for_k(input.iter().copied(), 3);
        let got = got.collect::<Vec<_>>();
        assert_eq!(
            got,
            vec![
                vec![10, 20, 30],
                vec![10, 20, 40],
                vec![10, 30, 40],
                vec![20, 30, 40]
            ]
        );

        let got = combine_for_k(input.iter().copied(), 4);
        let got = got.collect::<Vec<_>>();
        assert_eq!(got, vec![vec![10, 20, 30, 40]]);
    }
}
