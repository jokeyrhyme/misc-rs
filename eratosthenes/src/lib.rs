#![deny(clippy::all, clippy::pedantic, unsafe_code)]

/// generate prime numbers without recursion and without division/modulus/root,
/// in exchange for space-complexity of O(n) and time-complexity of O(n log log n),
/// see: [Sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)
pub fn sieve(limit: usize) -> impl Iterator<Item = usize> {
    let mut numbers: Vec<Option<usize>> = (2..=limit).map(Some).collect();

    for index in 0..(numbers.len()) {
        if let Some(candidate) = numbers[index] {
            let index_of_first_composite = index + candidate;
            // the original algorithm stops once index > square_root(limit),
            // but our implementation avoids square-root operations as an extra constraint,
            // so ours stops at index ~= limit/2 as a trade-off
            if index_of_first_composite >= numbers.len() {
                break;
            }
            let iterator = numbers[index_of_first_composite..]
                .iter_mut()
                .step_by(candidate);
            for composite in iterator {
                *composite = None;
            }
        }
    }

    numbers.into_iter().flatten()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sieve_101() {
        // see: https://en.wikipedia.org/wiki/List_of_prime_numbers
        let want = vec![
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83,
            89, 97, 101,
        ];
        let got: Vec<_> = sieve(101).collect();

        assert_eq!(got, want);
    }
}
