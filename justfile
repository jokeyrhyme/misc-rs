fmt:
	if command -v cargo >/dev/null; then cargo fmt --all; fi
	if command -v prettier >/dev/null; then prettier --list-different --write .; fi
	
test:
	if command -v alex >/dev/null; then alex; fi
	if command -v cargo >/dev/null; then cargo fmt --all --check; fi
	if command -v cargo >/dev/null; then cargo clippy --tests --workspace; fi
	if command -v cargo >/dev/null; then cargo test --workspace; fi
	if command -v prettier >/dev/null; then prettier --check .; fi
	if command -v write-good >/dev/null; then write-good *.md; fi
